#!/usr/bin/env python2

import os.path
import re
from _heapq import heappush
from _heapq import heappop
import argparse

import sys


# Global for indexing other lookup tables
# Could be improved!
raw_twitter_lines = []
debug_traces = False

def insertCost(index):
    return 1

def deleteCost(index):
    return 1

def substCost(source, target):
    if source == target:
        cost = 0
    else:
        cost = 1
        
    return cost

def  minEditDist(target, source):
    ''' Computes the min edit distance from target to source. Figure 3.25 in the book. Assume that
    insertions, deletions and (actual) substitutions all cost 1 for this HW. Note the indexes are a
    little different from the text. There we are assuming the source and target indexing starts a 1.
    Here we are using 0-based indexing.'''
    
    n = len(target)
    m = len(source)

    distance = [[0 for i in range(m+1)] for j in range(n+1)]

    for i in range(1,n+1):
        distance[i][0] = distance[i-1][0] + insertCost(target[i-1])

    for j in range(1,m+1):
        distance[0][j] = distance[0][j-1] + deleteCost(source[j-1])

    for i in range(1,n+1):
        for j in range(1,m+1):
            distance[i][j] = min(distance[i-1][j]+insertCost(target[i-1]),
                                 distance[i][j-1]+insertCost(source[j-1]),
                                 distance[i-1][j-1]+substCost(source[j-1],target[i-1]))
    return distance[n][m]

def CalculateWordErrorRate(min_edit_dist, num_words) :
    return min_edit_dist / num_words

# Here assume target is the gold standard 
def WordErrorRate(target, source):
    MED = minEditDist(target,source)
    num_words = len(target)
    WER = float(MED) / float(num_words)
    #print 'dividing ' + str(MED) + ' by ' + str(num_words) + ' = ' + str(WER) 
    return WER

def PopulateLexicon(filename, _restrict_lexicon):
    lexicon_limit = 75000 # for use if the lexicon is to be restricted
    word_re = re.compile('[^\s]+')
    count_re = re.compile('\d+$')
    lexicon = {}

    if os.path.isfile(filename):
        with open(filename) as f:
            content = f.readlines()
    
        line_num = 1;
        for line in content:
            if _restrict_lexicon and line_num > lexicon_limit:
                 break
# this per Handbook of NAtural LAnguage PRocessing and MAchine Translation - DARPA Global Autonomous Language Exploitation
# PAge 103 - "The performance of the Maxmatch algorithm is closely tied to the completeness of the dictionary"
            key = word_re.match(line)
            value = count_re.search(line)
            #print 'for line', line , key.group(), value.group()
            lexicon[key.group()] = value.group()
            line_num += 1
        
        print str(len(lexicon)) + ' lexicon entries read,'
        f.close()
    else:
        print 'Lexicon file does not exist'
        
    return lexicon

#return the populated test sentences as a lookup table on the hashtag
def PopulateTestData(filename):
    sentence_re = re.compile('[\w]+')
    test_sentences = {}

    if (os.path.isfile(filename)):
        with open(filename) as test_file:
            content = test_file.readlines()
            line_num = 1
            for line in content:
                raw_twitter_line = line.strip()
                raw_twitter_lines.append(raw_twitter_line)
                stripped = sentence_re.search(line).group()
                #print stripped + stripped.lower()
                test_sentences[raw_twitter_line] = (stripped.lower())
                
                line_num += 1
            
            print str(len(test_sentences)) + ' twitter hashtags read,'
            test_file.close()
    else:
        print 'Could not find test data file'
        
    return test_sentences

# populate a gold sentences as a lookup table on the hashtag           
def PopulateGolds(gold_filename):
    #Only process the lines that have been processed (had the hashtag removed - assume that this means that it has been processed!
    gold_standards = {}
    has_been_processed_re = re.compile('[\w]+')
    
    if (os.path.isfile(gold_filename)):
        with open(gold_filename) as f:
            content = f.readlines()
        
            line_num = 1
            for line in content:
                has_been_processed = has_been_processed_re.match(line)
                if  has_been_processed != None:
                    #print line + ' has been manually processed '
                    #reassemble the hashtag without the hash, so that we can index on it... very clumsy way of doing this!
                    #trimmed = re.sub(whitespace,'', line)
                    raw_twitter_line = raw_twitter_lines[line_num-1]
                    gold_standards[raw_twitter_line] = line.strip()
                    
                else:
                    #print 'For line - ' + line + ' has not been processed'
                    pass 
                
                line_num+=1
            print str(len(gold_standards)) + ' gold standards read,'
            f.close()
    else:
        print 'Could not find test data file'
        
    return gold_standards

# Run the maxmatch algorithm,returning the tokenization of the provided sentence
def MaxMatch(sentence, dictionary):
    if sentence == "":
        return []
    
    #start with the length of the word
    for substr_length in reversed(xrange(0,len(sentence)+1)):
        #print 'substr_length: ' + str(substr_length)
        first_word = sentence[0:substr_length]
        remainder = sentence[substr_length:len(sentence)]
        #print 'first word: ', first_word, ' remainder:', remainder
        if dictionary.has_key(first_word):
            #print 'Found ' +  first_word
                tokens = [first_word]
                next_token = MaxMatch(remainder,dictionary)
                #print 'want to append ' + str(tokens) + ' to ' + str(new_list)
                #for elem in new_list:
                #    tokens.append(elem)
                tokens += next_token
                #print tokens
                return tokens
        
    first_word = sentence[0]
    remainder = sentence[1:len(sentence)]  
    #print('fail test - first word: ' + first_word + ' remainder: ' + remainder)
    tokens = [first_word]
    next_token = MaxMatch(remainder,dictionary)
    #print 'want to append ' + str(tokens) + ' to ' +new_list
    tokens += next_token
    #print tokens
    return tokens

tokenizations = []

   
#Find all possible substrings
def SegmentString(_sentence, _lexicon):
    #print 'segmenting ' + _sentence        
    SegmentSubstr(_sentence, '', _lexicon)
    global tokenizations
    #Bandaid solution
    if not tokenizations:
        pair = (0.0, [_sentence])
        heappush(tokenizations, pair)

    (value, tokenization) = heappop(tokenizations)
    tokenizations = []
    return tokenization
        
#Take a list of words, and Score them, using a heuristic
# if the sentence is a single word, good! Score zero.
# for each word in the tokenized sentence,
#    add a Score of 1 if the length of a word is 1
#    add a Score of 2 if the word does not appear in the lexicon - the tokenizer probably couldn't find the word
#    add a small amount based on the average word length in the sentence. Longer words are preferred!
#     
def Score(_sentence, _lexicon):
    global debug_traces
    running_score = 0.0
    # if there is only one word, then the sentence was a single word - good!
    if len(_sentence) == 1:
        running_score = 0.0
    else:
        # count the number of single character words
        for word in _sentence:
            if len(word) == 1:
                running_score += 1.0
            else:
                if not _lexicon.has_key(word):
                    #print 'Found nonword '  + word
                    running_score += len(word)
                
        #print 'avg word length for sentence ' + str(_sentence) + str(sum(map(len,_sentence))/float(len(_sentence)))
        avgWordLength = sum(map(len,_sentence))/float(len(_sentence))
        running_score += 1.0 / avgWordLength
        if debug_traces:
            print 'Wordlist: ' + str(_sentence) + 'Score: ' + str(running_score) 
    
    return running_score

    

def SegmentSubstr(_sentence, _result, _lexicon):
    # Not a tidy implementation. We fill a global tokenizations and rely on the caller to clean this container, for each word...
    global tokenizations
    #HACK - stop it from recursing out of control - there is possibly a bug in the implementation somewhere, which I haven't had time to fix! It might just be the 
    # of permutations is too big... This is where a dynamic programming approach would be better than this recursive approach. It would make it Onm rather than On^2
    if len(tokenizations) > 500:
        return
    
    length = len(_sentence)
    #print 'Called util with ' + _sentence + ' start result: '  + _result
    for index in reversed(xrange(1, length+1)):
       
        prefix = _sentence[0:index]
        #print ' Iterating through ' + _sentence + ' index: ' + str(index) + ' calculated prefix ' + prefix
        # Note uses global lexicon!
        # if the word is found
        if _lexicon.has_key(prefix):
            # if it is the end of the sentence, push the progressive sentence + the new word, and a Score, onto the heap as a valid tokenization
            if index == length:
                _result += prefix
                tokens = _result.split()
                sentence_score = Score(tokens, _lexicon)
                pair = (sentence_score, tokens)
                heappush(tokenizations, pair)
                return
            # if we have not finished (returned) for this substring, prepare to go again for the smaller fragment of this string
            new_substring = _sentence[index:length]
            # prepare the sentence tokenization thus far
            progress_string = _result+prefix+' '
            #print 'Going to call util - new_substring: ' + new_substring  + ' length_to_check: ' + str(length_to_check) + ' progress_string ' + progress_string  + ' index ' + str(index)
            # the below is to handle words like #iphone6s, where the 6s at the end was not processed (due to not in dictionary), so no entry was every added into the tokenizations
            # we still need to add it however, even if this last bit is not in the lexicon
            tokens = (progress_string + new_substring).split() 
            sentence_score = Score(tokens, _lexicon)
            pair = (sentence_score, tokens)
            heappush(tokenizations, pair)
            
            # Call on the substring based on the backwards counting loop index
            SegmentSubstr(new_substring,progress_string, _lexicon)

#Start of the application

def main(argv):
    global debug_traces
    parser = argparse.ArgumentParser(description="MaxMatchPlus Cmdline Parser")
    parser.add_argument('-l','--lexicon', help='Lexicon file name',required=True)
    parser.add_argument('-t','--test',help='Test file name', required=True)
    parser.add_argument('-r','--ref',help='Reference file name', required=True)
    parser.add_argument('-i','--improved',help='Run improved algorithm?', action="store_true")
    parser.add_argument('-d','--debug',help='Debug traces?', action="store_true")
    args = parser.parse_args()
    print '###################'
    print ("Lexicon file: %s" % args.lexicon )
    print ("Test file: %s" % args.test )
    print ("Reference file: %s" % args.ref )
    print ("Improved algo?: %s" % args.improved )
    print ("Debug traces?: %s" % args.debug )
    print '###################'
    
    lexicon_filename = args.lexicon 
    test_filename = args.test
    gold_filename = args.ref
    output_filename = "sutcliffe-out-assgn1.txt"
    run_improved_maxmatch = args.improved
    debug_traces = args.debug
    
    if run_improved_maxmatch:
        print 'Populating full lexicon,'
        lexicon = PopulateLexicon(lexicon_filename, False)
    else:
        print 'Populating restricted lexicon,'
        lexicon = PopulateLexicon(lexicon_filename, True)
    testdata = PopulateTestData(test_filename)
    gold_standards = PopulateGolds(gold_filename)
    
    
    results_max = {}
    results_all = {}
    results = {}
    
    print 'Tokenizing hashtags...'
    
    if run_improved_maxmatch:
        print 'Using MaxMatchPlus'
        for sentence in raw_twitter_lines:
            results_all[sentence] = SegmentString(testdata[sentence], lexicon)
        results = results_all
    else:
        print 'Using MaxMatch'
        for sentence in raw_twitter_lines:
            results_max[sentence] = MaxMatch(testdata[sentence], lexicon)
        results = results_max
            
    

    conErrorRates = []
    print 'Calculating performance...'
    for sentence in raw_twitter_lines:
        if gold_standards.has_key(sentence):
            strProcessed = results[sentence]
            strGold = gold_standards[sentence].split()
            wer = WordErrorRate(strGold, strProcessed)
            pair = wer, sentence
            #print ('pair is ' + str(pair))
            conErrorRates.append(pair)
            # no need for a heap here? heapq.heappush(conErrorRates,pair)
            if debug_traces:
                print 'Processed String: ' + str(strProcessed) + ' - Gold String: ' + str(strGold) + ' WER: ' + str(wer)
    
    
    nErrorSum = 0
    for pair in conErrorRates:
        nErrorSum += (pair[0])
    
    nErrorAvg = nErrorSum / len(conErrorRates)
    
    print 'Finished calculations'
    print 'Average Word Error Rate: ' + str(nErrorAvg)    
    
    # It is useful for analysis, to show the worst cases, as well as the average
    conSortedErrorRates = sorted(conErrorRates, key=lambda wer: wer[0], reverse=True)
    nTopErrorsToShow = 10
    if len(conSortedErrorRates) < nTopErrorsToShow:
        nTopErrorsToShow = len(conSortedErrorRates)
    print 'Worst Word Error Rates: ' + str(conSortedErrorRates[:nTopErrorsToShow])      
    
    strSpace = " "
    print '###################'
    print 'Writing results to standard file...'
    with open(output_filename, 'w') as f:
        for sentence in raw_twitter_lines:
            #print 'Writing: ' + sentence
            strOutput = strSpace.join(results[sentence])
            f.write(strOutput + '\n')
        
        f.close()
        
    print 'Finished!'

if __name__ == "__main__":
    print "Running MaxMatchPlus"
    main(sys.argv[1:])