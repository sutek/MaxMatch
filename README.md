# MaxMatch
Author: Geoffrey Sutcliffe

Run using python2 sutcliffe-assgn1.py <arguments>

    usage: sutcliffe-assgn1.py [-h] -l LEXICON -t TEST -r REF [-i] [-d]
    -l supplies the lexicon file (mandatory)
    -t supplies the test set (mandatory)
    -r supplies the reference set (mandatory)
    -i uses the enhanced maxmatch algorithm (described below)
    -d enables some debugging traces

##Part 1 and Part 2
Implementation of MaxMatch is demonstrated as complete by comparing the output file (sutcliffe-out-assgn1.txt) against the reference output. There is no difference in the files.

The WER of the reference MaxMatch algorithm against the training set (hashtags-train-reference.txt), using the reference lexicon (bigwordslist.txt) is 0.66. This demonstrates the implementation of WordErrorRate and MinEditDistance.


##Part 3
The improvement is in two areas.

### The lexicon 
Removed single character words, and the second part of common contractions (such as ..ve, ..st, ..nt). Also, allowed the full lexicon through (not just the 75000 words)
### The algorithm
The improvement still uses a recursive algorithm. A better solution would be to use dynamic programming (see the discussion below).
At a high level, it generates all of the possible solutions and attempts to rank them using a coarse heuristic. It stores all of the possible tokenizations in a priority queue (a heap in python), with the lowest score at the top.
The reason for this solution is based on the observation that MaxMatch does well, but sometimes the agglutination of the longest word prevents the consideration of tokenization on smaller words early in the sentence, which may also prevent the generation of longer tokens later on in the sentence. The intent of this approach is that it provides a continuum where it is trying to find the best collection of the longest words it can find-- not just the long words at the beginning.

The problem with generating all of the possible solutions, with a large dictionary, is that the solution space is huge. I had to put a limit on the number of solutions returned. I set the limit to 500 solutions per twitter entry, and setting it to any higher did not improve the performance.  

The solution to this problem would be to implement the same principle using dynamic programming. It is a dynamic programming problem, because on the way to generating all of the possible solutions, we generate several subsolutions, and should be able to build on top of that in an efficient manner. 

The solutions generated were originally implemented to find words starting with the smallest string found, but I then realised that it was adviseable to keep the same principles of MaxMatch - prefer the biggest words that it can match. The result was about a 10% improvement with the test sets used (and is heavily effected by the paremeterized recursion limit hack).

One other change is that it can also return more than just a single character word that is not in the lexicon.  

The returned solutions will be ranked with the following heuristic:

    If there is only one word - return score of 0
    Else 
      For all words in the sentence
        if the length of the word is 1
          Increment score by 1 to punish those tokenizations that return lots of single character words
        else
          if the lexicon does not have the word
            Increment score by the length of the word to punish the tokenizations that return a large unknown word
      Increment score by 1 / average word length to reward sentences with long words

Note that the results are not normalized, but they are all relative and sufficient to preserve ranking in the priority queue.

##Test Results
On the test set initially provided (hashtags-dev.txt), I started providing a reference set, by processing the test set one-by-one (hashtags-dev-gold.txt). Only the entries that had the #hashtag removed would be counted for Word Error Rate calculations.

With the improvements to the algorithm and the lexicon, the improved score is 0.240945 (with the following command):

	./sutcliffe-assgn1.py -l bigwordlist_improved.txt -t hashtags-dev.txt -r hashtags-dev-gold.txt -i
